# Robotics Operating System 2: Masterclass

## Practical Lab: ROS System design


The class will organise in groups of 3 to investigate and design an additional solution for a ground robotics farm robot. This robot does automated inspection of crops to apply computer vision and machine learning models to evaluate the expected profit of the stock. The current system relies on ROS2 navigation capabilities (to have a full field coverage) but it wants to reduce the dependency of GPS system to accurately determine the robot's position.


This is an informal, highly speculative and high-level exercise that intends to encourage the students to creatively propose an innovative solution that can solve a real-world problem. The ROS2 and DDS allows for many different possibilities of solving problems and this activity intends to promote different perspectives and an overall reflection of all proposals.


## Goal

Design a system that collects a real-time minimum viable dataset that can be useful to accurately measure the robots' position.


### Functional requirements

- Robot must collect and process internal DDS;
- Robot must get accurate location;
- Robot must stream its location to the deployment base;

### Non-Functional requirements

- System must be able to operate without the need of GPS signal;
- System must operate with low latency;
- System design must integrate with ROS2;
- System design must leverage DDS technologies;


## Tasks

1. Define the high-level architecture of your ROS2 system;
2. Enumerate and justify the use of each ROS2 module;
3. Speculate on the DDS' standard data types that could be useful to produce the required real-time dataset;
4. Present you solution, for everyone else, for 3 minutes giving a brief plan of your system and how it will produce the real-time minimal viable dataset necessary.


The overall activity is planned to take a full hour and participants are expected to present their findings in after 50 minutes.


### Note
Due to technical and logistical constraints, a practical lab on Robotic Operating System version 2 (ROS2) is not feasible for the practical activity. The university MACS machines' operating system (CentOS 7) are not compatible with any of the ROS2 distributions.
